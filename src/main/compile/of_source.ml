open Trace
open Helpers

type file_path = string
type module_name = string

type c_unit = Buffer.t * (file_path * module_name) list

(* we should have on for filename with syntax_opt and one in case of no file *)
let extract_meta syntax file_name =
  let%bind syntax   = syntax_to_variant (Syntax_name syntax) (Some file_name) in
  ok @@ {syntax}

let make_meta syntax file_name_opt =
  let%bind syntax   = syntax_to_variant (Syntax_name syntax) file_name_opt in
  ok @@ {syntax}

let compile ~options ~meta (source_filename:string) : (c_unit , _) result =
  trace Main_errors.preproc_tracer @@ preprocess_file ~options ~meta source_filename

let compile_string ~options ~meta source : (c_unit , _) result =
  preprocess_string ~options ~meta source

let compile_contract_input : options:Compiler_options.t -> meta:meta -> string -> string -> (c_unit * c_unit , _) result =
    fun ~options ~meta storage parameter ->
  bind_map_pair (compile_string ~options ~meta) (storage,parameter)
