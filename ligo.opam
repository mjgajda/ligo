name: "ligo"
opam-version: "2.0"
maintainer: "Galfour <contact@ligolang.org>"
authors: [ "Galfour" ]
homepage: "https://gitlab.com/ligolang/tezos"
bug-reports: "https://gitlab.com/ligolang/tezos/issues"
synopsis: "A high-level language which compiles to Michelson"
dev-repo: "git+https://gitlab.com/ligolang/tezos.git"
license: "MIT"
# If you change the dependencies, run `opam lock` in the root
depends: [
  "odoc" { build }
  "ocamlfind" { build }
  "dune" { build & >= "2.7.1" & < "2.9" }
  "menhir" { = "20200624" }
  "bisect_ppx" {dev & >= "2.0.0"}
  "ocamlgraph"
  "coq" { build & >= "8.12" & < "8.13" }
  "ppx_let"
  "ppx_deriving"
  "ppx_deriving_yojson"
  "ppx_expect"
  "tezos-utils"
  "proto-alpha-utils"
  "yojson"
  "alcotest" { with-test & >= "1.0" & < "1.1" }
  "getopt"
  "terminal_size"
  "pprint"
  "ParserLib"
  "LexerLib"
  "UnionFind"
  "RedBlackTrees"
  # work around upstream in-place update
  "ocaml-migrate-parsetree" { = "1.4.0" }
  # work around tezos' failure to constrain
  "lwt" { = "5.3.0" }
]
pin-depends: [
  ["bisect_ppx.git" "git+https://github.com/aantron/bisect_ppx.git#02dfb10188033a26d07d23480c2bc44a3a670357"]
]
build: [
  [ "dune" "build" "-p" name "-j" jobs ]
]
